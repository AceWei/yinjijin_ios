//
//  main.m
//  yinjijin
//
//  Created by 魏诗豪 on 15/6/9.
//  Copyright (c) 2015年 AceWei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
